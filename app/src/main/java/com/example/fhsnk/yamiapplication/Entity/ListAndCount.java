package com.example.fhsnk.yamiapplication.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@Entity(tableName = "list_table")
public class ListAndCount {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String word;

    @Nullable
    @ColumnInfo(name = "count")
    private int count;

    public ListAndCount(@NonNull String word, @Nullable int count) {this.word = word;this.count = count;}

    @NonNull
    public String getWord() {
        return this.word;
    }

    @Nullable
    public int getCount() {
        return this.count;
    }
}