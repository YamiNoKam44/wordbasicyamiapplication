package com.example.fhsnk.yamiapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.example.fhsnk.yamiapplication.Dao.UserDao;
import com.example.fhsnk.yamiapplication.Databases.AppDatabase;
import com.example.fhsnk.yamiapplication.Entity.User;

public class RegisterActivity extends AppCompatActivity {

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mPasswordRepeatView;
    AppDatabase appDatabase;

                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.activity_register);
                    appDatabase = AppDatabase.getDatabase(this);
                    mEmailView = (AutoCompleteTextView) findViewById(R.id.email_reg);
                    mPasswordView = (EditText) findViewById(R.id.password);
                    mPasswordRepeatView = (EditText) findViewById(R.id.repeat_password);
                    Button mReginisterButton = (Button) findViewById(R.id.register_user_button);
                    mReginisterButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!validate()) return;
                            setUser();
                Intent login = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });
        Button mBackButton = (Button) findViewById(R.id.back);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });
    }

    private boolean validate() {
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordRepeat = mPasswordRepeatView.getText().toString();
        if (!TextUtils.isEmpty(password) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(passwordRepeat))
            return false;
        if (!password.equals(passwordRepeat))
            return false;
        if (!isPasswordValid(password) || !isEmailValid(email))
            return false;
        return true;
    }

    private void setUser() {
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordRepeat = mPasswordRepeatView.getText().toString();
        User user = new User();
        user.setLogin(email);
        user.setPassword(password);
        appDatabase.userDao().insert(user);
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }
}
