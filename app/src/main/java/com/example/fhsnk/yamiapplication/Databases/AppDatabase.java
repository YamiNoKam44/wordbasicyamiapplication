package com.example.fhsnk.yamiapplication.Databases;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.fhsnk.yamiapplication.Dao.UserDao;
import com.example.fhsnk.yamiapplication.Dao.WordDao;
import com.example.fhsnk.yamiapplication.Entity.ListAndCount;
import com.example.fhsnk.yamiapplication.Entity.User;

@Database(entities = {ListAndCount.class, User.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract WordDao wordDao();

    public abstract UserDao userDao();

    private static AppDatabase INSTANCE;


    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "database").allowMainThreadQueries()
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}