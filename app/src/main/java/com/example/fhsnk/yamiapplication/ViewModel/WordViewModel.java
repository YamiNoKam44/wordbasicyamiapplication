package com.example.fhsnk.yamiapplication.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.fhsnk.yamiapplication.Entity.ListAndCount;
import com.example.fhsnk.yamiapplication.Repository.WordRepository;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository mRepository;

    private LiveData<List<ListAndCount>> mAllWords;

    public WordViewModel (Application application) {
        super(application);
        mRepository = new WordRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    public LiveData<List<ListAndCount>> getAllWords() { return mAllWords; }

    public void insert(ListAndCount listAndCount) { mRepository.insert(listAndCount); }
}