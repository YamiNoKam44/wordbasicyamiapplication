package com.example.fhsnk.yamiapplication.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.fhsnk.yamiapplication.Dao.UserDao;
import com.example.fhsnk.yamiapplication.Databases.AppDatabase;
import com.example.fhsnk.yamiapplication.Entity.User;

import java.util.List;

public class UserRepository {
    private UserDao mUserDao;
    private LiveData<List<User>> mAllUsers;

    public UserRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mUserDao = db.userDao();
        mAllUsers = mUserDao.getAllUsers();
    }

    public LiveData<List<User>> getAllUsers() {
        return mAllUsers;
    }


    public void insert (User user) {
        new UserRepository.insertAsyncTask(mUserDao).execute(user);
    }

    public AsyncTask<User, User, User> getUser (String name) {
        User user = new User();
        user.setLogin(name);
        return new UserRepository.getAsyncTask(mUserDao).execute(user);

    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        insertAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class getAsyncTask extends AsyncTask<User, User, User> {

        private UserDao mAsyncTaskDao;

        getAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected User doInBackground(final User... params) {
            return mAsyncTaskDao.getUserByLogin(params.getClass().getName());
        }

    }
}
