package com.example.fhsnk.yamiapplication.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.fhsnk.yamiapplication.Entity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insert(User user);

    @Query("DELETE FROM users WHERE login = :login")
    void deleteUser(String login);

    @Query("SELECT login, password from users ORDER BY login ASC")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT login, password FROM users where login = :login")
    User getUserByLogin(String login);
}
