package com.example.fhsnk.yamiapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fhsnk.yamiapplication.Entity.ListAndCount;
import com.example.fhsnk.yamiapplication.R;

import java.util.List;

public class ListAndCountListAdapter extends RecyclerView.Adapter<ListAndCountListAdapter.WordViewHolder> {

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView listAndCountItemView;

        private WordViewHolder(View itemView) {
            super(itemView);
            listAndCountItemView = itemView.findViewById(R.id.textView);
        }
    }

    private final LayoutInflater mInflater;
    private List<ListAndCount> mListAndCounts; // Cached copy of words

    public ListAndCountListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        if (mListAndCounts != null) {
            ListAndCount current = mListAndCounts.get(position);
            String text = current.getWord();
            holder.listAndCountItemView.setText(text);
        } else {
            holder.listAndCountItemView.setText("No ListAndCount");
        }
    }

    public void setWords(List<ListAndCount> listAndCounts){
        mListAndCounts = listAndCounts;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mListAndCounts has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mListAndCounts != null)
            return mListAndCounts.size();
        else return 0;
    }
}