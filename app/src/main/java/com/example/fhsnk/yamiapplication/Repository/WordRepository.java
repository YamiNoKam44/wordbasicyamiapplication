package com.example.fhsnk.yamiapplication.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.fhsnk.yamiapplication.Dao.WordDao;
import com.example.fhsnk.yamiapplication.Databases.AppDatabase;
import com.example.fhsnk.yamiapplication.Entity.ListAndCount;

import java.util.List;

public class WordRepository {

    private WordDao mWordDao;
    private LiveData<List<ListAndCount>> mAllWords;

    public WordRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getAllWords();
    }

    public LiveData<List<ListAndCount>> getAllWords() {
        return mAllWords;
    }


    public void insert (ListAndCount listAndCount) {
        new insertAsyncTask(mWordDao).execute(listAndCount);
    }

    private static class insertAsyncTask extends AsyncTask<ListAndCount, Void, Void> {

        private WordDao mAsyncTaskDao;

        insertAsyncTask(WordDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ListAndCount... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}