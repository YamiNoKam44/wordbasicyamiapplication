package com.example.fhsnk.yamiapplication.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.fhsnk.yamiapplication.Entity.ListAndCount;

import java.util.List;

@Dao
public interface WordDao {

    @Insert
    void insert(ListAndCount listAndCount);

    @Query("DELETE FROM list_table")
    void deleteAll();

    @Query("SELECT word,count from list_table ORDER BY word ASC")
    LiveData<List<ListAndCount>> getAllWords();
}